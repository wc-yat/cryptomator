rm -rf dist
git restore dist
cd dist/linux/appimage
./build.sh
cp cryptomator-SNAPSHOT-x86_64.AppImage ~/.local/bin/cryptomator
cp Cryptomator.AppDir/Cryptomator.desktop ~/.local/share/applications
